package main

import (
	// "fmt"
	"math"
	"time"
	"github.com/banthar/Go-SDL/sdl"
	"github.com/banthar/Go-SDL/gfx"
	"github.com/go-gl/gl"
	"bitbucket.org/spark4160/gophysics/physics"
)

func main() {
	title := "go-physics"
	width := 1300
	height := 740
	fwidth := float64(width)
	fheight := float64(height)

	// SDL
	sdl.Init(sdl.INIT_VIDEO)
	defer sdl.Quit()

	sdl.GL_SetAttribute(sdl.GL_SWAP_CONTROL, 1)

	if sdl.SetVideoMode(width, height, 32, sdl.OPENGL) == nil {
		panic("sdl error")
	}

	if gl.Init() != 0 {
		panic("gl error")
	}

	sdl.WM_SetCaption(title, title)

	gl.Enable(gl.TEXTURE_2D)
	gl.Viewport(0, 0, width, height)
	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Ortho(0, fwidth, fheight, 0, -1, 1)

	gl.ClearColor(0, 0, 0, 0)

	// FPS
	fps := gfx.NewFramerate()
	fps.SetFramerate(50)

	// Timing
	this_time := time.Now().UnixNano()
	last_time := this_time - 1
	ticktime := 0.0
	// totaltime := 0.0

	// Physics
	p := new(physics.Physics)
	p.Init(fwidth, fheight)
	timescale := 2.0

	// Testing
	frames := uint32(0)
	// Walls
	// p.NewRect(fwidth/2, 32, 0, math.Inf(+1), fwidth - 32, 32)
	// p.NewRect(fwidth/2, fheight - 32, 0, math.Inf(+1), fwidth - 32, 32)
	// p.NewRect(fwidth - 32, fheight/2, math.Pi/2, math.Inf(+1), fheight - 98, 32)
	// p.NewRect(32, fheight/2, math.Pi/2, math.Inf(+1), fheight - 98, 32)
	// Objects
	objA := p.NewSquare(fwidth/2 - 32, 32, 0, 1, 32)
	objB := p.NewSquare(fwidth/2 + 32, fheight - 32, 0, 1, 32)

	// PID

	// X
	lasterrorx := 0.0
	integralx := 0.0
	// Zero
	Kpx,	Kix,	Kdx := 0.0, 0.0, 0.0
	// Manual
	// Kpx,	Kix,	Kdx := 4.0, 1.5, 2.0

	// Y
	lasterrory := 0.0
	integraly := 0.0
	// Manual
	Kpy,	Kiy,	Kdy := 4.0, 1.5, 2.0
	// Classic
	// Kpy,	Kiy,	Kdy := 1.5, 2.14, -0.26
	// Pessen
	// Kpy,	Kiy,	Kdy := 1.75, 0.5, -0.3675
	// No overshoot
	// Kpy,	Kiy,	Kdy := 0.5, 0.714, -0.233

	// Tuning
	// lastdiff := 0.0
	// cycletime := time.Now().UnixNano()
	// amp := 0.0

	running := true
	for running {
		// Timing
		this_time = time.Now().UnixNano()
		ticktime = float64(this_time - last_time)*math.Pow10(-9)*timescale
		last_time = this_time
		// fmt.Printf("Tick time: %.2fms\n", ticktime)
		// totaltime = totaltime + ticktime
		// fmt.Printf("Time: %.2fms\n", totaltime)

		// Input
		// Continuous
		sx, sy, sr := objB.GetPosition()
		keystates := sdl.GetKeyState()
		if keystates[sdl.K_UP] == 1 {
			objB.ForceMoment(sx, sy,  500*math.Sin(sr),  500*math.Cos(sr))
		}
		if keystates[sdl.K_DOWN] == 1 {
			objB.ForceMoment(sx, sy, -500*math.Sin(sr), -500*math.Cos(sr))
		}
		if keystates[sdl.K_LEFT] == 1 {
			objB.ForceMoment(sx, sy, -500*math.Cos(sr), -500*math.Sin(sr))
			// objB.Moment(-1000)
		}
		if keystates[sdl.K_RIGHT] == 1 {
			objB.ForceMoment(sx, sy,  500*math.Cos(sr),  500*math.Sin(sr))
			// objB.Moment( 1000)
		}
		for e := sdl.PollEvent(); e != nil; e = sdl.PollEvent() {
			switch e.(type) {
				case *sdl.QuitEvent:
					running = false
				case *sdl.KeyboardEvent:
					kbe := e.(*sdl.KeyboardEvent)
					if kbe.Type == sdl.KEYDOWN {
						switch kbe.Keysym.Sym {
							case sdl.K_ESCAPE:
								running = false
						}
					}
			}
		}

		// Physics
		p.SetTicktime(ticktime)
		// Gravity
		p.ForceAll(0, -9.81)
		p.DragAll(0.2)
		// Testing
			Ax, Ay, _ := objA.GetPosition()
			Bx, By, _ := objB.GetPosition()
			// X
			errorx := Bx - Ax
			diffx := (errorx - float64(lasterrorx))/ticktime
			lasterrorx = errorx
			integralx += errorx*ticktime
			objA.Force(errorx*Kpx + integralx*Kix + diffx*Kdx, 0)
			// Y
			errory := By - Ay
			diffy := (errory - float64(lasterrory))/ticktime
			lasterrory = errory
			integraly += errory*ticktime
			objA.Force(0, errory*Kpy + integraly*Kiy + diffy*Kdy)
			// if frames%fps.GetFramerate() == 0 {
			// 	fmt.Printf("Errors: %.2f, %.2f\n", errorx, errory)
			// 	fmt.Printf("Props: %.2f -> %.2f, %.2f -> %.2f, \n", errorx, errorx*Kpx, errory, errory*Kpy)
			// 	fmt.Printf("Integrals: %.2f -> %.2f, %.2f -> %.2f\n", integralx, integralx*Kix, integraly, integraly*Kiy)
			// 	fmt.Printf("Diffs: %.2f -> %.2f, %.2f -> %.2f\n", diffx, diffx*Kdx, diffy, diffy*Kdy)
			// 	objA.Print()
			// 	objB.Print()
			// }
			// Tuning
			// if errory > amp {
			// 	amp = errory
			// }
			// if lastdiff >= 0 && diffy <= 0 {
			// 	period := float64(time.Now().UnixNano() - cycletime)/1000000
			// 	fmt.Printf("Period: %.2fms\n", period)
			// 	fmt.Printf("Amp: %.4f\n", amp)
			// 	cycletime = time.Now().UnixNano()
			// 	amp = 0.0
			// }
			// lastdiff = diffy;
		//Tick
		p.Tick()
		p.ResetForces()
		p.Collide()
		p.Tick()

		// SDL
		gl.Clear(gl.COLOR_BUFFER_BIT)
		p.Render()
		sdl.GL_SwapBuffers()

		// Clock
		fps.FramerateDelay()
		frames++
	}
}