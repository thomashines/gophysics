package physics

import (
	"fmt"
	"math"
	"github.com/go-gl/gl"
)

const (
	PARTICLE	= 0
	CONVEX		= 1
)

var names = []string{
	"Particle",
	"Convex Polygon",
}

// Structs
type Vec2 struct {
	x, y float64
}

type Vec3 struct {
	x, y, z float64
}

type Primitive struct {
	ptype int
	colour *Vec3
	vertexcount int
	basevertices []*Vec2
	lastvertices []*Vec2
	vertices []*Vec2
	ticktime float64
	mass, inertia, restitution float64
	x, y, roll float64
	lastx, lasty, lastroll float64
	dx, dy, droll float64
	ddx, ddy, ddroll float64
}

// gl
func drawCircle(width float64, height float64, x float64, y float64, radius float64, sides int) {
	gl.Begin(gl.LINE_LOOP)
	for a := 0.0; a < 2*math.Pi; a += 2*math.Pi/float64(sides) {
		gl.Vertex2d(x + math.Sin(a)*radius, height - y - math.Cos(a)*radius)
	}
	gl.End()
}

func drawVertices(width float64, height float64, vertices []*Vec2) {
	if len(vertices) > 1 {
		gl.Begin(gl.LINE_LOOP)
		for i := 0; i < len(vertices); i++ {
			gl.Vertex2d(vertices[i].x, height - vertices[i].y)
		}
		gl.End()
	}
}

func (p *Primitive) Render(width float64, height float64) {
	gl.Color3d(p.colour.x, p.colour.y, p.colour.z)
	drawCircle(width, height, p.x, p.y, 4, 8)
	drawVertices(width, height, p.vertices)
}

// physics
func NewPrimitive(x float64, y float64, roll float64, mass float64) (*Primitive) {
	p := Primitive{
		vertexcount: 0,
		ptype: PARTICLE,
		x: x,
		y: y,
		roll: roll,
		lastx: x,
		lasty: y,
		lastroll: roll,
		dx: 0,
		dy: 0,
		droll: 0,
		ddx: 0,
		ddy: 0,
		ddroll: 0,
		mass: mass,
		inertia: 0,
	}
	p.colour = &Vec3{x: 0.0, y: 1.0, z: 0.0}
	return &p
}

func (p *Primitive) Print() {
	fmt.Printf("%s: x:%.2f y:%.2f roll:%.2f\n", names[p.ptype], p.x, p.y, p.roll)
}

func (p *Primitive) GetPosition() (float64, float64, float64) {
	return p.x, p.y, p.roll
}

func (p *Primitive) Tick(ticktime float64) {
	// Motion
	p.lastx = p.x
	p.lasty = p.y
	p.lastroll = p.roll
	p.x += p.dx*ticktime
	p.y += p.dy*ticktime
	p.roll += p.droll*ticktime
	p.dx += p.ddx*ticktime
	p.dy += p.ddy*ticktime
	p.droll += p.ddroll*ticktime

	// Vertices
	for i := 0; i < p.vertexcount; i++ {
		p.lastvertices[i].x = p.vertices[i].x
		p.lastvertices[i].y = p.vertices[i].y
		p.vertices[i].x = p.x + p.basevertices[i].x*math.Cos(p.roll) + p.basevertices[i].y*math.Sin(p.roll)
		p.vertices[i].y = p.y - p.basevertices[i].x*math.Sin(p.roll) + p.basevertices[i].y*math.Cos(p.roll)
	}
}

func (p *Primitive) ResetForces() {
	p.ddx = 0
	p.ddy = 0
	p.ddroll = 0
}

func (p *Primitive) Force(fx float64, fy float64) (bool) {
	if p.mass > 0 {
		p.ddx += fx/p.mass
		p.ddy += fy/p.mass
		return true
	}
	return false
}

func (p *Primitive) Moment(m float64) (bool) {
	if p.inertia > 0 {
		p.ddroll += m/p.inertia
		return true
	}
	return false
}

func (p *Primitive) ForceMoment(x float64, y float64, fx float64, fy float64) (bool) {
	if p.Force(fx, fy) {
		return p.Moment((x - p.x)*fy + (y - p.y)*fx)
	}
	return false
}

func (p *Primitive) Drag(drag float64) {
	// F = -drag*v^2
	p.ddx += -drag*p.dx
	p.ddy += -drag*p.dy
}

// Shapes
func (p *Primitive) Rect(width float64, height float64) {
	p.vertexcount = 4
	p.basevertices = make([]*Vec2, p.vertexcount)
	p.basevertices[0] = &Vec2{x: -width/2, y: -height/2};
	p.basevertices[1] = &Vec2{x:  width/2, y: -height/2};
	p.basevertices[2] = &Vec2{x:  width/2, y:  height/2};
	p.basevertices[3] = &Vec2{x: -width/2, y:  height/2};
	p.vertices = make([]*Vec2, p.vertexcount)
	p.lastvertices = make([]*Vec2, p.vertexcount)
	for i := 0; i < p.vertexcount; i++ {
		p.vertices[i] = &Vec2{
			x: p.x + p.basevertices[i].x*math.Cos(p.roll) + p.basevertices[i].y*math.Sin(p.roll),
			y: p.y - p.basevertices[i].x*math.Sin(p.roll) + p.basevertices[i].y*math.Cos(p.roll),
		}
		p.lastvertices[i] = &Vec2{
			x: p.x + p.basevertices[i].x*math.Cos(p.roll) + p.basevertices[i].y*math.Sin(p.roll),
			y: p.y - p.basevertices[i].x*math.Sin(p.roll) + p.basevertices[i].y*math.Cos(p.roll),
		}
	}
	p.inertia = p.mass*(math.Pow(width, 2) + math.Pow(height, 2))/12
}

// Makers
func NewRect(x float64, y float64, roll float64, mass float64, width float64, height float64) *Primitive {
	p := NewPrimitive(x, y, roll, mass)
	p.Rect(width, height)
	p.ptype = CONVEX
	return p
}

func NewSquare(x float64, y float64, roll float64, mass float64, sidelength float64) *Primitive {
	p := NewPrimitive(x, y, roll, mass)
	p.Rect(sidelength, sidelength)
	p.ptype = CONVEX
	return p
}