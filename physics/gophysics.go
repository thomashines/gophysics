package physics

import (
	// "fmt"
	"math"
)

type Physics struct {
	width, height float64
	ticktime float64
	objects []*Primitive
}

func (p *Physics) Init(width float64, height float64) {
	p.width = width
	p.height = height
	p.ticktime = 1
	p.objects = make([]*Primitive, 0)
}

// SDL
func (p *Physics) Render() {
	for i := 0; i < len(p.objects); i++ {
		p.objects[i].Render(p.width, p.height)
	}
}

// Physics
// Ticking
func (p *Physics) SetTicktime(ticktime float64) {
	p.ticktime = ticktime
}

func (p *Physics) Collide() {
	for a := 0; a < len(p.objects); a++ {
		p.objects[a].colour.x = 0.0
		p.objects[a].colour.y = 1.0
		p.objects[a].colour.z = 0.0
	}
	for a := 0; a < len(p.objects); a++ {
		for b := a + 1; b < len(p.objects); b++ {
			if p.objects[a].ptype == CONVEX && p.objects[b].ptype == CONVEX {
				if CollideConvexPolygon(p.objects[a], p.objects[b]) {
					// fmt.Printf("Collision: %d with %d\n", a, b)
					p.objects[a].colour.x = 1.0
					p.objects[a].colour.y = 0.0
					p.objects[a].colour.z = 0.0
					p.objects[b].colour.x = 1.0
					p.objects[b].colour.y = 0.0
					p.objects[b].colour.z = 0.0
					// Resolve impulse
				}
			}
		}
	}
}

func CollideConvexPolygon(objA *Primitive, objB *Primitive) (bool) {
	// Objects to check for collisions
	// Currently max 2
	obj := []*Primitive{
		objA,
		objB,
	}
	for oo := 0; oo < 2; oo++ {
		for a := 1; a < obj[oo].vertexcount; a++ {
			// Axis to check for overlap on
			// This is perpendicular to the current edge
			axis := &Vec2{
				x: obj[oo].vertices[a-1].y - obj[oo].vertices[a].y,
				y: obj[oo].vertices[a].x - obj[oo].vertices[a-1].x,
			}
			// Find the minimum and maximum projections of points along this axis
			min := []float64{
				math.Inf(+1),
				math.Inf(+1),
			}
			max := []float64{
				math.Inf(-1),
				math.Inf(-1),
			}
			for oi := 0; oi < 2; oi++ {
				for v := 0; v < obj[oi].vertexcount; v++ {
					// Scalar projection of the vertex onto the axis
					// Is not mathematically exact, but multiplied by the magnitude of the axis vector
					scal := obj[oi].vertices[v].x*axis.x + obj[oi].vertices[v].y*axis.y
					if scal > max[oi] {
						max[oi] = scal
					}
					if scal < min[oi] {
						min[oi] = scal
					}
				}
			}
			// If they are not overlapping, then it is impossible for there to be a collision
			if min[0] > max[1] || max[0] < min[1] {
				return false
			}
		}
	}
	return true
}

// Collisions
func (p *Physics) Tick() {
	for i := 0; i < len(p.objects); i++ {
		p.objects[i].Tick(p.ticktime)
	}
}

//Forces
func (p *Physics) ResetForces() {
	for i := 0; i < len(p.objects); i++ {
		p.objects[i].ResetForces()
	}
}

func (p *Physics) ForceAll(fx float64, fy float64) {
	for i := 0; i < len(p.objects); i++ {
		p.objects[i].Force(fx, fy)
	}
}

func (p *Physics) DragAll(drag float64) {
	for i := 0; i < len(p.objects); i++ {
		p.objects[i].Drag(drag)
	}
}

// Objects
func (p *Physics) NewRect(x float64, y float64, roll float64, mass float64, width float64, height float64) *Primitive {
	r := NewRect(x, y, roll, mass, width, height)
	p.objects = append(p.objects, r)
	return r
}

func (p *Physics) NewSquare(x float64, y float64, roll float64, mass float64, sidelength float64) *Primitive {
	s := NewSquare(x, y, roll, mass, sidelength)
	p.objects = append(p.objects, s)
	return s
}